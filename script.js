'use strict';

/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?

	за допомогою подї keydown та event.code() або event.key(). 

2. Яка різниця між event.code() та event.key()?

	event.code визначає код клавіші, а event.key визначає символ, який друкується натисканням цієї клавіші. У спеціальних клавіш event.code та event.key майже однакові.

3. Які три події клавіатури існує та яка між ними відмінність?

	event.keydown - коли клавіша натискається, event.keyup - коли клавіша відпускається, event.repeat - коли клавіша затиснута і event.keydown повторюється багато разів.

*/

/* 
Практичні завдання
Реалізувати функцію підсвічування клавіш.
Технічні вимоги:
- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
*/

const blockLetter = document.querySelectorAll('.btn');
	
document.addEventListener('keydown', (event) => {
	
	blockLetter.forEach((element) => {
		const toLowEvent = event.key.toLowerCase();
		const toLowElement = element.innerText.toLowerCase();

		if (toLowEvent === toLowElement) {
			element.classList.add('active');
		} else {
			element.classList.remove('active');
		};
	});
});
